# SVG Text animation

![alt text][gif]

[gif]: designer.gif "Animation gif"

- Create a text and convert it to SVG
  * Removed the filled color and add a border
  * Make sure the SVG has multiple paths for each letters when exported.
  * Put your SVG in index.html
- Add styling to your SVG
  * Position your SVG wherever you want to and
  * Add this line: ```stroke-linecap: square;```
  * This makes sure the entire stroke gets filled with the animation
- Run the animateSvg function in index.js
   * First param = id of your svg
   * Second param = delay before the animation starts
   * Third param = delay between animation of each letter
- Enjoy :)


