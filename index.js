document.addEventListener('DOMContentLoaded', () => {
  function animateSvg (id, delay, delayIncrement){
    const logo = document.getElementById(id);
    // We take direct path children to not get the ones from the mask
    const logoPaths = document.querySelectorAll(`#${id} > path`);

    for(let i = 0; i < logoPaths.length; i++){
      // Getting the values
      logoPaths[i].style.strokeDasharray  = logoPaths[i].getTotalLength() + "px";
      logoPaths[i].style.strokeDashoffset = logoPaths[i].getTotalLength() + "px";
      // Adding the animation (set in CSS file)
      logoPaths[i].style.animation = `line-anim 2s ease forwards ${delay}s`;
      delay += delayIncrement;
    }
    // Adding the fill animation (set in CSS file)
    logo.style.animation = `fill 0.5s ease forwards ${delay}s`;
  }


// Set the id of SVG, delay time in seconds to start animation and delay between each animation
  animateSvg('logo', 1, 0.8)
}, false);
